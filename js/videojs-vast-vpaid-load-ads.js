(function ($, Drupal, window, document) {
  Drupal.behaviors.videojsVastVpaid = {
    attach: function (context, settings) {
      var registerPlugin = videojs.registerPlugin || videojs.plugin;

      /**
       * Register the ad integration plugin.
       * To initialize for a player, call player.videojsVastVpaid().
       *
       * @param {mixed} opts Hash of options for the diretteWebVastAds plugin.
      */
      videojs.registerPlugin('videojsVastVpaid', function (opts) {
        var player = this;
        var adsCancelTimeout = 10000;

        switch (opts.source) {
          case 'revive':
            var options = {
            //Media tag URL
              "adTagXML": function(done) {
                var xhttp = new XMLHttpRequest();

                xhttp.onreadystatechange = function() {
                  if (this.readyState == 4 && this.status == 200) {
                    done(null, this.responseText);
                  } else {
                    /* error check */
                  }
                }

                xhttp.open("GET", opts.url, true);
                xhttp.send();
              },
              playAdAlways: true,
              //Note: As requested we set the preroll timeout at the same place than the adsCancelTimeout
              adCancelTimeout: adsCancelTimeout,
              adsEnabled: !!opts.adsEnabled
            };
            break;
          };
        var vastAd = player.vastClient(options);
    });
    player.videojsVastVpaid({
      source: Drupal.settings.videojs_vast_vpaid.source,
      url: Drupal.settings.videojs_vast_vpaid.url,
      adsEnabled: true,
    });
  }
}

})(jQuery, Drupal, this, this.document);
